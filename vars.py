import os

API_ENDPOINT = os.environ.get('API_ENDPOINT') 
API_USER = os.environ.get('API_USER')
API_KEY = os.environ.get('API_KEY')
API_CLOUDFLARE_ENDPOINT = os.environ.get('API_CLOUDFLARE_ENDPOINT')
API_CLOUDFLARE_TOKEN = os.environ.get('API_CLOUDFLARE_TOKEN')
CLOUDFLARE_ZONE_ID = os.environ.get('CLOUDFLARE_ZONE_ID')
PBX_IP = os.environ.get('PBX_IP')
PBX_DOMAIN = ""

