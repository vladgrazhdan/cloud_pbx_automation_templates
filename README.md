# Multi-tenant IP PBX call flow setup automation 

## Intro

Python based automation script for setting up a tenant for IP PBX. Integrates with Cloudflare Managed DNS service via Cloudflare API.

## Overview

*   [x] **Can be modified and extended**
*   [x] **Tested**

### Requirements

* Python 3.10+

### Install

Use git to clone this repository locally.

### Usage

The script can be integrated with LibreOffice Calc which can serve a role of a GUI. The scripts retrievs all API endponts from environment variables. The list of variables is in the vars.py file.
